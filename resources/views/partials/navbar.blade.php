<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
        <img class="img-fluid" src="{{ asset('img/logo1.jpg') }}" width="150" height="70" alt="logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('blogs.index') }}">Blogs <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href=" {{ route('blog.categories.index') }}">Blog-Categories </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('videos.index') }}">Videos</a>
            </li>

            <li class="nav-item active">
                <a class="nav-link" href="{{ route('partners.index') }}">Partners</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('clients.index') }}">Clients</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('volunteers.index') }}">Volunteers</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('contacts.index') }}">Contacts</a>
            </li>

            <li class="nav-item active">
                <a class="nav-link" href="{{ route('clients.donations') }}">Donations</a>
            </li>
        </ul>

    </div>

    <!-- Settings -->

    <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                {{Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ asset('profile') }}">Profile</a>
                <div class="dropdown-divider"></div>
                <form method="POST" action="{{ asset('logout') }}" class="form-inline my-2 my-lg-0">
                    @csrf
                    <button class="dropdown-item" type="submit">{{Auth()->user()->name}} Logout</button>
                </form>
            </div>
        </li>
    </ul>
</nav>