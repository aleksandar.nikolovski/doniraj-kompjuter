@extends('partials.main')


@section('css')
<link rel="stylesheet" href=" {{ asset('css/index.css') }}">
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div id="clock" class="text-center display-1"></div>
            <div id="greeting" class="text-center display-2 mt-5 text-white"></div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function getCurrentTime() {
        let date = new Date();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();

        hours = (hours < 10 ? '0' : '') + hours;
        minutes = (minutes < 10 ? '0' : '') + minutes;
        seconds = (seconds < 10 ? '0' : '') + seconds;

        return hours + ':' + minutes + ':' + seconds;
    }

    function showGreetings() {
        let date = new Date();
        let hours = date.getHours();
        let greeting = '';

        if (hours >= 0 && hours < 12) {
            greeting = 'Good morning';
        } else if (hours >= 12 && hours < 18) {
            greeting = 'Good afternoon';
        } else {
            greeting = 'Good evening';
        }

        return greeting;
    }

    function updateClock() {
        let clockElement = document.getElementById('clock');
        let greetingElement = document.getElementById('greeting');

        clockElement.innerHTML = getCurrentTime();
        greetingElement.innerHTML = showGreetings();
    }

    setInterval(updateClock, 1000);
</script>
@endsection