@extends('partials.main')


@section('title', 'Contacts')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <h4>Contacts</h4>
                </div>
                <div class="col-12 mt-5">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error')}}
                    </div>
                    @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                    @endif

                    <div class="d-flex align-items-center justify-content-end">
                        <div class="dropdown ml-5">
                            <button class="btn  dropdown-toggle text-light bg-dark" type="button" data-toggle="dropdown" aria-expanded="false">
                                Contact Status
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('contacts.index', ['search' => '1']) }}">Active</a>
                                <a class="dropdown-item" href="{{ route('contacts.index', ['search' => '0']) }}">Inactive</a>
                                <a class="dropdown-item" href="{{ route('contacts.index', ['search' => 'archived']) }}">Archihed/Invalid</a>
                            </div>
                        </div>
                        <form action="{{ route('contacts.index') }}">
                            <div class="form-group ml-5">
                                <label for="startDate">Start Date:</label>
                                <input type="date" class="form-control" id="startDate" name="startDate" style="width: 50%; display: inline-block">
                            </div>
                            <div class="form-group ml-5">
                                <label for="endDate">End Date:</label>
                                <input type="date" class="form-control" id="endDate" name="endDate" style="width: 50%; display: inline-block">
                                <button type="submit" class="btn text-light bg-dark">Find</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Surname</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Message</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $contacts as $contact )
                            <tr>
                                <th>{{$contact->name}}</th>
                                <th>{{$contact->surname}}</th>
                                <th>{{$contact->email}}</th>
                                <th>{{$contact->phone}}</th>
                                <th>{{$contact->message}}</th>
                                <th class="text-nowrap">{{$contact->created_at->format('d-m-Y')}}</th>
                                @if($contact->deleted_at == NULL)
                                @if($contact->status == 1)
                                <th>
                                    <form action=" {{ route('contacts.inactive', $contact->id) }} " method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-success">Active</button>
                                    </form>
                                </th>
                                @else
                                <th>
                                    <form action=" {{ route('contacts.active', $contact->id) }} " method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-danger">Inactive</button>
                                    </form>
                                </th>
                                @endif
                                @endif
                                @if($contact->deleted_at == NULL)
                                <th>
                                    <form action="{{ route('contacts.destroy', $contact->id) }} " method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-info">Archive</button>
                                    </form>
                                </th>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection