@extends('partials.main')


@section('title', 'Volunteers')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <h4>Volunteers</h4>
                </div>
                <div class="col-12 mt-5">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error')}}
                    </div>
                    @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                    @endif

                    <div class="d-flex align-items-center justify-content-end">
                        <div class="dropdown ml-5">
                            <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                Volunteer Status
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('volunteers.index', ['search' => '1']) }}">Active</a>
                                <a class="dropdown-item" href="{{ route('volunteers.index', ['search' => '0']) }}">Inactive</a>
                                <a class="dropdown-item" href="{{ route('volunteers.index', ['search' => 'archived']) }}">Archihed/Invalid</a>
                            </div>
                        </div>
                        <form action="{{ route('volunteers.index') }}">
                            <div class="form-group ml-5">
                                <label for="startDate">Start Date:</label>
                                <input type="date" class="form-control" id="startDate" name="startDate" style="width: 50%; display: inline-block">
                            </div>
                            <div class="form-group ml-5">
                                <label for="endDate">End Date:</label>
                                <input type="date" class="form-control" id="endDate" name="endDate" style="width: 50%; display: inline-block">
                                <button type="submit" class="btn btn-warning">Find</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">City</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Motivation</th>
                                <th scope="col">Document</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $volunteers as $volunteer )
                            <tr>
                                <th>{{$volunteer->full_name}}</th>
                                <th>{{$volunteer->city}}</th>
                                <th>{{$volunteer->email}}</th>
                                <th>{{$volunteer->phone}}</th>
                                <th>{{$volunteer->motivation}}</th>
                                <th><a href="/storage/{{$volunteer->document}}" target="#">{{$volunteer->document}}</a></th>
                                <th class="text-nowrap">{{$volunteer->created_at->format('d-m-Y')}}</th>
                                @if($volunteer->deleted_at == NULL)
                                @if($volunteer->status == 1)
                                <th>
                                    <form action=" {{ route('volunteers.inactive', $volunteer->id) }} " method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-success">Active</button>
                                    </form>
                                </th>
                                @else
                                <th>
                                    <form action=" {{ route('volunteers.active', $volunteer->id) }} " method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-danger">Inactive</button>
                                    </form>
                                </th>
                                @endif
                                @endif
                                @if($volunteer->deleted_at == NULL)
                                <th>
                                    <form action="{{ route('volunteers.destroy', $volunteer->id) }} " method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-info">Archive</button>
                                    </form>
                                </th>
                                @endif
                            </tr>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection