@extends('partials.main')



@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Update partner</p>
                    <a href=" {{ route('partners.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action=" {{ route('partners.update', $partner->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{old('name', $partner->name)}}" placeholder="Partner name">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="url">Website</label>
                        <input type="text" class="form-control @error('website') is-invalid @enderror" name="website" id="website" value="{{old('website', $partner->website)}}" placeholder="Website URL">
                        @error('website')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="partner_type_id">Partner Type</label>
                        <select name="partner_type_id" id="partner_type_id" class="form-control @error('partner_type_id') is-invalid @enderror">
                            <option value="-1">Select type of partner...</option>
                            @foreach($partnerTypes as $partnerType)
                            <option value="{{ $partnerType->id }}" @if((old('partner_type_id') !=null && old('partner_type_id')==$partnerType->id) || ($partner->partner_type_id == $partnerType->id )) selected @endif>{{ $partnerType->name }}</option>
                            @endforeach
                        </select>
                        @error('partner_type_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                        <img src=" {{ asset('storage/' . $partner->image) }}" class="img-fluid" height="100px" width="100px"></img>
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection