@extends('partials.main')


@section('title', 'Partners')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <h4>Partners</h4>
                </div>
                <div class="col-12 mt-5">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error')}}
                    </div>
                    @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                    @endif
                    <a href=" {{ route('partners.create') }}" class="btn btn-info">Create a new partner</a>
                </div>
                <div class="col-10 offset-1">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">name</th>
                                <th scope="col">image</th>
                                <th scope="col">website</th>
                                <th scope="col">type</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $partners as $partner )
                            <tr>
                                <th>{{$partner->id}}</th>
                                <th>{{$partner->name}}</th>
                                <th><a href="storage/{{$partner->image}}" target="#"><img src=" {{ asset('storage/' . $partner->image) }}" class="" height="100px" width="200px"></img></a></th>
                                <th class="text-wraps"><a href="{{$partner->website}}" target="#">{{$partner->website}}</a></th>
                                <th>{{$partner->partnerType->name}}</th>
                                <th><a href=" {{ route('partners.edit', $partner->id) }}" class="btn btn-outline-warning">Edit</a> </th>
                                <th>
                                    <form id="delete-form" action=" {{ route('partners.destroy', $partner->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger delete">Delete</button>
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $(".delete").click(function(e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure you want to delete this partner?",
                text: "- By deleting it all the information relating to this partner will be deleted aswell",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete the partner!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#delete-form').submit();
                }
            });
        });
    });
</script>
@endsection