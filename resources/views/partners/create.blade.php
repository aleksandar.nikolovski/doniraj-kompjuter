@extends('partials.main')


@section('title', 'Create partners')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Create a new partner</p>
                    <a href=" {{ route('partners.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                @if(Session::has('error'))
                <div class="alert alet-danger">
                    {{ Session::get('error')}}
                </div>
                @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
                @endif
                <form action=" {{ route('partners.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{old('name')}}" placeholder="Partner name">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="url">Website</label>
                        <input type="text" class="form-control @error('website') is-invalid @enderror" name="website" id="website" value="{{old('website')}}" placeholder="Website URL">
                        @error('website')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="partner_type_id">Partner Type</label>
                        <select name="partner_type_id" id="partner_type_id" class="form-control @error('partner_type_id') is-invalid @enderror">
                            <option value="-1">Select type of partner...</option>
                            @foreach($partnerTypes as $partnerType)
                            <option value="{{ $partnerType->id }}" @if(old('partner_type_id') !=null && old('partner_type_id')==$partnerType->id) selected @endif>{{ $partnerType->name }}</option>
                            @endforeach
                        </select>
                        @error('partner_type_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection