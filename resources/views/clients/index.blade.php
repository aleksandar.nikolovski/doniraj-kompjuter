@extends('partials.main')


@section('title', 'Clients')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <h4>Clients</h4>
                    </div>
                </div>
                <div class="col-12 mt-5">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error')}}
                    </div>
                    @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                    @endif

                    <div class="d-flex align-items-center">

                        <a href=" {{ route('clients.create') }} " class="btn btn-info">Create a new client</a>

                        <div class="dropdown ml-5">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                Status
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('clients.index', ['search' => '1']) }}">Active</a>
                                <a class="dropdown-item" href="{{ route('clients.index', ['search' => '0']) }}">Complete</a>
                                <a class="dropdown-item" href="{{ route('clients.index', ['search' => 'archived']) }}">Archihed/Invalid</a>
                            </div>
                        </div>
                        <form action="{{ route('clients.index') }}">
                            <div class="form-group ml-5">
                                <label for="startDate">Start Date:</label>
                                <input type="date" class="form-control" id="startDate" name="startDate" style="width: 50%; display: inline-block">
                            </div>
                            <div class="form-group ml-5">
                                <label for="endDate">End Date:</label>
                                <input type="date" class="form-control" id="endDate" name="endDate" style="width: 50%; display: inline-block">
                                <button type="submit" class="btn btn-secondary">Find</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-6">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">City</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Equipment</th>
                                <th scope="col">Donated_equipment</th>
                                <th scope="col">Document1</th>
                                <th scope="col">Document2</th>
                                <th scope="col">Comment</th>
                                <th scope="col">Date</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $clients as $client )
                            <tr>
                                <th>{{$client->name}} {{$client->surname}}</th>
                                <th>{{$client->city}}</th>
                                <th>{{$client->email}}</th>
                                <th class="text-nowrap">{{$client->phone}}</th>
                                <th>{{$client->equipment}}</th>
                                <th>{{$client->donated_equipment}}</th>
                                <th class="text-wrap "><a href="{{ asset('storage/'. $client->file1 )}}" target="#"> {{ $client->file1}}</a></th>
                                <th><a href="storage/{{$client->file2}}" target="#">{{ $client->file2}}</a></th>
                                <th class="">{{$client->comment}}</th>
                                <th class="text-nowrap">{{$client->created_at->format('d-m-Y')}}</th>
                                @if($client->deleted_at == NULL)
                                @if($client->status == 1)
                                <th>
                                    <form action=" {{ route('clients.complete', $client->id) }} " method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-success">Active</button>
                                    </form>
                                </th>
                                @else
                                <th>
                                    <form action=" {{ route('clients.active', $client->id) }} " method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-danger">Completed</button>
                                    </form>
                                </th>
                                @endif
                                @endif
                                @if($client->deleted_at == NULL)
                                <th>
                                    <form action="{{ route('clients.destroy', $client->id) }} " method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-info">Archive</button>
                                    </form>
                                </th>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection