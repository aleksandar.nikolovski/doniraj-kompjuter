@extends('partials.main')


@section('title', 'Donations')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h4>Donations</h4>

            <div class="col-12 mt-5">
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error')}}
                </div>
                @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
                @endif
                <div class="dropdown mb-5">
                    <button class="btn btn-secondary dropdown-toggle w-25" type="button" data-toggle="dropdown" aria-expanded="false">
                        Active Clients
                    </button>
                    <div class="dropdown-menu w-25">
                        @foreach ($activeClients as $activeCLient )
                        <a class="dropdown-item" href="{{ route('clients.donations', ['active_client_id' => $activeCLient->id]) }}">{{ $activeCLient->name}} {{ $activeCLient->surname}}</a>
                        @endforeach
                    </div>
                </div>

                <form action=" {{ route('clients.donate') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="donation">Donation: @if(isset($donationClient)) <h4>to {{$donationClient->name }} {{$donationClient->surname }}</h4> @endif</label>
                        @if(isset($donationClient))
                        <input type="text" class="form-control" name="donation" id="donation" value="{{ $donationClient->donated_equipment}}">
                        <input type="hidden" class="form-control" name="id" id="id" value="{{ $donationClient->id}}">
                        @else
                        <input type="text" class="form-control" name="donation" id="donation">
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Donate</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection