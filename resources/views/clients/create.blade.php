@extends('partials.main')


@section('title', 'Create clients')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Create a new Blog</p>
                    <a href=" {{ route('clients.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                @if(Session::has('error'))
                <div class="alert alet-danger">
                    {{ Session::get('error')}}
                </div>
                @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
                @endif
                <form action=" {{ route('clients.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ old('name') }}" placeholder="Client name">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="surname">Surname</label>
                        <input type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" id="surname" value="{{old('surname')}}" placeholder="Client surname">
                        @error('surname')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control @error('city') is-invalid @enderror" name="city" id="city" value="{{old('city')}}" placeholder="City">
                        @error('city')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{old('email')}}" placeholder="Email">
                        @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" value="{{old('phone')}}" placeholder="Phone">
                        @error('phone')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="equipment">Equipment</label>
                        <input type="text" class="form-control @error('equipment') is-invalid @enderror" name="equipment" id="equipment" value="{{old('equipment')}}" placeholder="Equipment">
                        @error('equipment')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="file1">File1<span class="text-danger">*</span></label>
                        <input type="file" name="file1" id="file1" class="form-control @error('file1') is-invalid @enderror">
                        @error('file1')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="file2">File2</label>
                        <input type="file" name="file2" id="file2" class="form-control @error('file2') is-invalid @enderror">
                        @error('file2')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="comment">Comment</label>
                        <textarea id="comment" name="comment" class="form-control shadow-sm rounded @error('comment') is-invalid @enderror" rows="3" placeholder="comment">{{ old('comment') }}</textarea>
                        @error('text')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection