@extends('partials.main')



@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row justify-content-between">
                <div class=" col-4 offset-1">
                    <div class="card-header">
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="mb-0">Edit Category</p>
                            <a href=" {{route('blog.categories.index') }}" class="btn btn-info">Back</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('blog.categories.update', $category) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{old('name', $category->name)}}" placeholder="Category name">
                                @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-success">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection