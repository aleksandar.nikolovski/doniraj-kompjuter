@extends('partials.main')


@section('title', 'Blog categories')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row justify-content-between">
                <div class=" col-4 offset-1">
                    <div class="card-header">
                        <h4>Create a new Category</h4>
                    </div>
                    <div class="card-body">
                        @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error')}}
                        </div>
                        @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success')}}
                        </div>
                        @endif
                        <form action="{{ route('blog.categories.store') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{old('name')}}" placeholder="Category name">
                                @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>

                <div class="col-4">
                    <div class="row">
                        <div class="col-7">
                            <h4>Categories</h4>
                        </div>

                        <div class="col-6">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">Category</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ( $categories as $category )
                                    <tr>
                                        <th>{{ $category->id}}</th>
                                        <th>{{ $category->name}}</th>
                                        <th><a href=" {{ route('blog.categories.edit', $category->id) }}" class="btn btn-outline-warning">Edit</a> </th>
                                        <th>
                                            <form id="delete-form" action="{{ route('blog.categories.destroy', $category) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-outline-danger delete">Delete</button>
                                            </form>
                                        </th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $(".delete").click(function(e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure you want to delete this category ?",
                text: "- By deleting it all the information relating to this category will be deleted aswell",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete the category!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#delete-form').submit();
                }
            });
        });
    });
</script>
@endsection