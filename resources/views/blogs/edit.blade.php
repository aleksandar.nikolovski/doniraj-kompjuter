@extends('partials.main')



@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Edit Blog</p>
                    <a href=" {{route('blogs.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action=" {{ route('blogs.update', $blog) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" value="{{old('title', $blog->title)}}" placeholder="Blog title">
                        @error('title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="text">Text</label>
                        <textarea id="text" name="text" class="form-control shadow-sm rounded @error('text') is-invalid @enderror" rows="3" placeholder="Blog content">{{ old('text', $blog->text) }}</textarea>
                        @error('text')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category_id" id="category_id" class="form-control @error('category_id') is-invalid @enderror">
                            <option value="-1">Select category...</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if((old('category_id') !=null && old('category_id')==$category->id) || ($category->id == $blog->category_id)) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                        <img src=" {{ asset('storage/' . $blog->image) }}" class="img-thumbnail" width="100px" height="100px" alt="image"></img>
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection