@extends('partials.main')


@section('title', 'Create blogs')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Create a new Blog</p>
                    <a href=" {{ route('blogs.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                @if(Session::has('error'))
                <div class="alert alet-danger">
                    {{ Session::get('error')}}
                </div>
                @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
                @endif
                <form action=" {{ route('blogs.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" value="{{old('title')}}" placeholder="Blog title">
                        @error('title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="text">Text</label>
                        <textarea id="text" name="text" class="form-control shadow-sm rounded @error('text') is-invalid @enderror" rows="3" placeholder="Blog content">{{ old('text') }}</textarea>
                        @error('text')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category_id" id="category_id" class="form-control @error('category_id') is-invalid @enderror">
                            <option value="-1">Select category...</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if(old('category_id') !=null && old('category_id')==$category->id) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection