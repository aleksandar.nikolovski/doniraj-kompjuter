@extends('partials.main')


@section('title', 'Blog posts')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <h4>Blogs</h4>
                </div>
                <div class="col-12 mt-5">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error')}}
                    </div>
                    @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                    @endif
                    <a href=" {{ route('blogs.create') }}" class="btn btn-info">Create a new blog</a>
                </div>
                <div class="col-10 offset-1">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">category</th>
                                <th scope="col">title</th>
                                <th scope="col">image</th>
                                <th scope="col">text</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $blogs as $blog )
                            <tr>
                                <th>{{$blog->id}}</th>
                                <th>{{$blog->category->name}}</th>
                                <th class="text-wrap">{{$blog->title}}</th>
                                <th><a href="storage/{{$blog->image}}" target="#"><img src=" {{ asset('storage/' . $blog->image) }}" class="img-fluid"></img></a></th>
                                <th class="truncate-column">{{$blog->text}}</th>
                                <th>{{$blog->created_at}}</th>
                                <th><a href=" {{ route('blogs.edit', $blog->id) }}" class="btn btn-outline-warning">Edit</a> </th>
                                <th>
                                    <form id="delete-form" action=" {{ route('blogs.destroy', $blog->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger delete">Delete</button>
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $(".delete").click(function(e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure you want to delete this blog ?",
                text: "- By deleting it all the information relating to this blog will be deleted aswell",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete the blog!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#delete-form').submit();
                }
            });
        });
    });
</script>
@endsection