@extends('partials.main')


@section('title', 'Create videos')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Create a new Video</p>
                    <a href=" {{ route('videos.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                @if(Session::has('error'))
                <div class="alert alet-danger">
                    {{ Session::get('error')}}
                </div>
                @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success')}}
                </div>
                @endif
                <form action=" {{ route('videos.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" id="url" value="{{old('url')}}" placeholder="Video url">
                        @error('url')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection