@extends('partials.main')


@section('title', 'Videos/Interviews')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <h4>Videos</h4>
                </div>
                <div class="col-12 mt-5">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error')}}
                    </div>
                    @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success')}}
                    </div>
                    @endif
                    <a href=" {{ route('videos.create') }}" class="btn btn-info">Create a new video</a>
                </div>
                <div class="col-10 offset-1">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">image</th>
                                <th scope="col">URL</th>
                                <th scope="col">date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $videos as $video )
                            <tr>
                                <th>{{$video->id}}</th>
                                <th><a href="storage/{{$video->image}}" target="#"><img src=" {{ asset('storage/' . $video->image) }}" class="" height="100px" width="200px"></img></a></th>
                                <th class="text-wraps"><a href="{{$video->url}}" target="#">{{$video->url}}</a></th>
                                <th>{{$video->created_at}}</th>
                                <th><a href=" {{ route('videos.edit', $video->id) }}" class="btn btn-outline-warning">Edit</a> </th>
                                <th>
                                    <form id="delete-form" action=" {{ route('videos.destroy', $video->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger delete">Delete</button>
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $(".delete").click(function(e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure you want to delete this video?",
                text: "- By deleting it all the information relating to this video will be deleted aswell",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete the video!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#delete-form').submit();
                }
            });
        });
    });
</script>
@endsection