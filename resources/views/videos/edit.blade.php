@extends('partials.main')



@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <p class="mb-0">Edit Video</p>
                    <a href=" {{ route('videos.index') }}" class="btn btn-info">Back</a>
                </div>
            </div>
            <div class="card-body">
                <form action=" {{ route('videos.update', $video->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" id="url" value="{{old('url', $video->url)}}" placeholder="Video url">
                        @error('url')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid @enderror">
                        <img src=" {{ asset('storage/' . $video->image) }}" class="img-thumbnail" width="100px" height="100px" alt="image"></img>
                        @error('image')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-success">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection