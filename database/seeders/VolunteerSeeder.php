<?php

namespace Database\Seeders;

use App\Models\Volunteer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VolunteerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $volunteers = [
            [
                'full_name' => 'John Doe',
                'city' => 'Skopje',
                'email' => 'john.doe@hotmail.com',
                'phone' => '070159263',
                'motivation' => 'I am highly motivated individual',
                'document' => 'uploads/94557e796810d9dc8e03ee6b2818755b.pdf',
                'status' => '1',
            ],
            [
                'full_name' => 'Jane Doe',
                'city' => 'Veles',
                'email' => 'jane.doe@hotmail.com',
                'phone' => '070123456',
                'motivation' => 'Do what you can!',
                'document' => 'uploads/a68219eca65746e08228f9f0d8d4d210.jpg',
                'status' => '1',
            ],
            [
                'full_name' => 'Alice Burton',
                'city' => 'Ohrid',
                'email' => 'alice.burton@hotmail.com',
                'phone' => '075123456',
                'motivation' => 'Just do it! Think afterwards',
                'document' => 'uploads/a68219eca65746e08228f9f0d8d4d210.jpg',
                'status' => '0',
            ],
            [
                'full_name' => 'Sam Burton',
                'city' => 'Debar',
                'email' => 'sam.burton@hotmail.com',
                'phone' => '075123456',
                'motivation' => 'computer',
                'document' => 'uploads/a68219eca65746e08228f9f0d8d4d210.jpg',
                'status' => '0',
            ]
        ];

        foreach ($volunteers as $bolunteer) {
            Volunteer::create($bolunteer);
        }
    }
}
