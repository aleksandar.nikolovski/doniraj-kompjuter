<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        for ($i = 0; $i < 10; $i++) {
            Client::Create([
                'name' =>  fake()->firstName(),
                'surname' => fake()->lastName(),
                'city' => fake()->city(),
                'email' => fake()->email(),
                'phone' => fake()->phoneNumber(),
                'equipment' => 'mouse',
                'comment' => fake()->words(10, true),
                'file1' => 'uploads/94557e796810d9dc8e03ee6b2818755b.pdf',
                'status' => fake()->boolean(50),
            ]);
        }
    }
}
