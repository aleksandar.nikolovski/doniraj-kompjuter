<?php

namespace Database\Seeders;

use App\Models\PartnerType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PartnerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $types = ['International', 'Regional', 'Official'];
        
        for ($i = 0; $i < count($types); $i++) {
            PartnerType::create([
                'name' => $types[$i]
            ]);
        }
    }
}
