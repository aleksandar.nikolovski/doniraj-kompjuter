<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::all();

        return view('blog-categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request)
    {
        $category = new Category();

        $category->name = $request->name;

        if ($category->save()) {
            return redirect()->route('blog.categories.index')->with(['success' => 'The category has been created']);;
        }

        return redirect()->route('blog.categories.index')->with(['error' => 'Something went wrong']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {

        return view('blog-categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;

        if ($category->save()) {
            return redirect()->route('blog.categories.index')->with(['success' => 'The category has been updated']);;
        }

        return redirect()->route('blog.categories.index')->with(['error' => 'Something went wrong']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        if ($category->delete()) {
            return redirect()->route('blog.categories.index')->with(['success' => 'The category has been deleted']);;
        }

        return redirect()->route('blog.categories.index')->with(['error' => 'Something went wrong']);;
    }
}
