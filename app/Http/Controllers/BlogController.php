<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Support\Facades\File;
use App\Http\Requests\StoreBlogRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateBlogRequest;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $blogs = Blog::all();

        return view('blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();

        return view('blogs.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBlogRequest $request)
    {

        $blog = new Blog();
        $blog->title = $request->title;
        $blog->text = $request->text;
        $blog->category_id = $request->category_id;


        $image = $request->file('image');
        $fileName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
        $filePath = 'uploads/' . $fileName;
        Storage::disk('public')->put($filePath, file_get_contents($request->file('image')));
        $blog->image = $filePath;

        if ($blog->save()) {
            return redirect()->route('blogs.create')->with(['success' => 'The Blog has been created']);
        }

        return redirect()->route('blogs.create')->with(['error' => 'Something went wrong']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog)
    {
        $categories = Category::all();
        return view("blogs.edit", compact(['blog', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $blog->title = $request->title;
        $blog->text = $request->text;
        $blog->category_id = $request->category_id;


        if ($request->hasfile('image')) {

            if (Storage::disk('public')->exists($blog->image)) {
                Storage::disk('public')->delete($blog->image);
            }

            $image = $request->file('image');
            $fileName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $filePath = 'uploads/' . $fileName;
            Storage::disk('public')->put($filePath, file_get_contents($request->file('image')));
            $blog->image = $filePath;
        }

        if ($blog->save()) {
            return redirect()->route('blogs.index')->with(['success' => 'The blog has been updated']);
        }

        return redirect()->route('blogs.index')->with(['error' => 'Something went wrong']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        if (Storage::disk('public')->exists($blog->image)) {
            Storage::disk('public')->delete($blog->image);
        }

        if ($blog->delete()) {
            return redirect()->route('blogs.index')->with('success', 'The blog has been deleted successfuly');
        }

        return redirect()->route('blogs.index')->with('error', 'Something went wrong...');
    }
}
