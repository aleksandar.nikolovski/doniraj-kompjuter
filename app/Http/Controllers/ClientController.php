<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\DonateClientRequest;
use App\Http\Requests\UpdateClientRequest;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {

        if ($request->has('startDate')) {

            $startDate = Carbon::parse($request->startDate);
            $edDate = Carbon::parse($request->endDate);

            $clients = Client::where('created_at', '<=', $edDate)
                ->where('created_at', '>=', $startDate)
                ->get();

            return view('clients.index', compact('clients'));
        }

        $search = $request->search;
        if ($search == 'archived') {
            $clients = Client::onlyTrashed()->get();
            return view('clients.index', compact('clients'));
        }

        if ($search == '1' || $search == '0') {
            $clients = Client::where('status', $search)->get();
            return view('clients.index', compact('clients'));
        }

        $clients = Client::withTrashed()->get();
        return view('clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreClientRequest $request)
    {
        $client = new Client();

        $client->name = $request->name;
        $client->surname = $request->surname;
        $client->city = $request->city;
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->equipment = $request->equipment;
        $client->file2 = $request->file2;
        $client->comment = $request->comment;

        $file1 = $request->file('file1');
        $fileName = md5($file1->getClientOriginalName() . time()) . '.' . $file1->getClientOriginalExtension();
        $filePath = 'uploads/' . $fileName;
        Storage::disk('public')->put($filePath, file_get_contents($request->file('file1')));
        $client->file1 = $filePath;

        if ($request->file2) {
            $file2 = $request->file('file2');
            $fileName = md5($file2->getClientOriginalName() . time()) . '.' . $file2->getClientOriginalExtension();
            $filePath = 'uploads/' . $fileName;
            Storage::disk('public')->put($filePath, file_get_contents($request->file('file2')));
            $client->file2 = $filePath;
        }

        if ($client->save()) {
            return redirect()->route('clients.create')->with(['success' => 'The client has been created']);;
        }

        return redirect()->route('clients.create')->with(['error' => 'Something went wrong']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClientRequest $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Client $client)
    {

        $client->status = 0;
        $client->save();
        if ($client->delete()) {

            return redirect()->route('clients.index')->with(['success' => 'The client has been archived']);;
        }
        return redirect()->route('clients.index')->with(['error' => 'Something went wrong']);
    }

    public function active(Client $client)
    {

        $client->status = 1;
        if ($client->save()) {

            return redirect()->route('clients.index')->with(['success' => 'The client has been set to active']);;
        }
        return redirect()->route('clients.index')->with(['error' => 'Something went wrong']);
    }

    public function complete(Client $client)
    {

        $client->status = 0;
        if ($client->save()) {

            return redirect()->route('clients.index')->with(['success' => 'The client`s status is set to completed']);;
        }
        return redirect()->route('clients.index')->with(['error' => 'Something went wrong']);
    }

    public function donations(DonateClientRequest $request)
    {

        if ($request->has('active_client_id')) {

            $activeClients = Client::where('status', '1')->get();

            $donationClient = Client::where('id', $request->active_client_id)->first();

            return view('clients.donations', compact('activeClients', 'donationClient'));
        }

        $activeClients = Client::where('status', '1')->get();

        return view('clients.donations', compact('activeClients'));
    }

    public function donate(Request $request)
    {

        if ($request->donation != NULL) {
            $donationClient = Client::where('id', $request->id)->first();

            $donationClient->donated_equipment = $request->donation;

            if ($donationClient->save()) {
                return redirect()->route('clients.donations')->with(['success' => 'Donation was successful!']);
            }
        }

        return redirect()->route('clients.donations')->with(['error' => 'Donation was NOT successful!']);
    }
}
