<?php

namespace App\Http\Controllers\Api;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Videos = Video::paginate(5);

        return response()->json(['Videos' => $Videos]);
    }
}
