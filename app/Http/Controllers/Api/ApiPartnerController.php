<?php

namespace App\Http\Controllers\Api;

use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PartnerType;

class ApiPartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($type = NULL)
    {
        if ($type) {
            $partnerTypeId = PartnerType::where('name', $type)->first()->id;

            if ($partnerTypeId) {
                $partners = Partner::where('partner_type_id', $partnerTypeId)->paginate(5);
                return response()->json(['partners' => $partners]);
            } else {
                return response()->json(['error' => 'Invalid partner type!']);
            }
        }
        $partners = Partner::paginate(5);

        return response()->json(['partners' => $partners,]);
    }
}
