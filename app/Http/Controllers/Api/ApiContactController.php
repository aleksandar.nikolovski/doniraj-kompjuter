<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContactRequest;

class ApiContactController extends Controller
{
    public function store(StoreContactRequest $request)
    {
        $contact = new Contact();

        $contact->name = $request->name;
        $contact->surname = $request->surname;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;

        if ($contact->save()) {
            return response()->json(['success' => 'Contact form is sent']);;
        }

        return response()->json(['error' => 'Something went wrong!']);;
    }
}
