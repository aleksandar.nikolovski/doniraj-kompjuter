<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $completedDonations = Client::where('status', '0')->count();

        $partners = Partner::all()->count();

        return response()->json(['DonationCount' => $completedDonations, 'Partners' => $partners]);
    }
}
