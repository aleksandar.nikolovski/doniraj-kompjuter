<?php

namespace App\Http\Controllers\Api;

use App\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreVolunteerRequest;

class ApiVolunteerController extends Controller
{
    public function store(StoreVolunteerRequest $request)
    {

        $volunteer = new Volunteer();

        $volunteer->full_name = $request->full_name;
        $volunteer->city = $request->city;
        $volunteer->email = $request->email;
        $volunteer->phone = $request->phone;
        $volunteer->motivation = $request->motivation;

        if ($request->document) {
            $document = $request->file('document');
            $fileName = md5($document->getClientOriginalName() . time()) . '.' . $document->getClientOriginalExtension();
            $filePath = 'uploads/' . $fileName;
            Storage::disk('public')->put($filePath, file_get_contents($request->file('document')));
            $volunteer->document = $filePath;
        }

        if ($volunteer->save()) {
            return response()->json(['success' => 'Volunteer application has been submitted']);;
        }

        return response()->json(['error' => 'Something went wrong, try again!']);
    }
}
