<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreClientRequest;

class ApiClientController extends Controller
{
    public function store(StoreClientRequest $request)
    {

        $client = new Client();

        $client->name = $request->name;
        $client->surname = $request->surname;
        $client->city = $request->city;
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->equipment = $request->equipment;
        $client->file2 = $request->file2;
        $client->comment = $request->comment;

        $file1 = $request->file('file1');
        $fileName = md5($file1->getClientOriginalName() . time()) . '.' . $file1->getClientOriginalExtension();
        $filePath = 'uploads/' . $fileName;
        Storage::disk('public')->put($filePath, file_get_contents($request->file('file1')));
        $client->file1 = $filePath;

        if ($request->file2) {
            $file2 = $request->file('file2');
            $fileName = md5($file2->getClientOriginalName() . time()) . '.' . $file2->getClientOriginalExtension();
            $filePath = 'uploads/' . $fileName;
            Storage::disk('public')->put($filePath, file_get_contents($request->file('file2')));
            $client->file2 = $filePath;
        }

        if ($client->save()) {
            return response()->json(['success' => 'Your applicaiton is successful!']);;
        }

        return response()->json(['error' => 'Something went wrong, try again!']);
    }
}
