<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiTokenController extends Controller
{
    public function index()
    {
        $user = User::where('name', 'test')->first();
        $token = $user->createToken('api_key')->plainTextToken;

        return response()->json(['token' => $token]);
    }
}
