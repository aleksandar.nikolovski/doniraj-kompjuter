<?php

namespace App\Http\Controllers\Api;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiBlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::paginate(10);

        return response()->json(['blogs' => $blogs]);
    }
}
