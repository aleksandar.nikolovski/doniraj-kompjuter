<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $videos = Video::all();
        return view('videos.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('videos.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreVideoRequest $request)
    {
        $video = new Video();
        $video->url = $request->url;

        $image = $request->file('image');
        $fileName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
        $filePath = 'uploads/' . $fileName;
        Storage::disk('public')->put($filePath, file_get_contents($request->file('image')));
        $video->image = $filePath;

        if ($video->save()) {
            return redirect()->route('videos.create')->with(['success' => 'The Video has been created']);
        }

        return redirect()->route('videos.create')->with(['error' => 'Something went wrong']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Video $video)
    {
        return view('videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateVideoRequest $request, Video $video)
    {
        $video->url = $request->url;

        if ($request->hasfile('image')) {

            if (Storage::disk('public')->exists($video->image)) {
                Storage::disk('public')->delete($video->image);
            }

            $image = $request->file('image');
            $fileName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $filePath = 'uploads/' . $fileName;
            Storage::disk('public')->put($filePath, file_get_contents($request->file('image')));
            $video->image = $filePath;
        }

        if ($video->save()) {
            return redirect()->route('videos.index')->with(['success' => 'The video has been updated']);
        }

        return redirect()->route('videos.index')->with(['error' => 'Something went wrong']);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Video $video)
    {
        if (Storage::disk('public')->exists($video->image)) {
            Storage::disk('public')->delete($video->image);
        }

        if ($video->delete()) {
            return redirect()->route('videos.index')->with('success', 'The video has been deleted successfuly');
        }

        return redirect()->route('videos.index')->with('error', 'Something went wrong...');
    }
}
