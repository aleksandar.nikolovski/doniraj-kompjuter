<?php

namespace App\Http\Controllers;

use App\Models\Volunteer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class VolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {

        if ($request->has('startDate')) {

            $startDate = Carbon::parse($request->startDate);
            $edDate = Carbon::parse($request->endDate);

            $volunteers = Volunteer::where('created_at', '<=', $edDate)
                ->where('created_at', '>=', $startDate)
                ->get();

            return view('volunteers.index', compact('volunteers'));
        }

        $search = $request->search;
        if ($search == 'archived') {
            $volunteers = Volunteer::onlyTrashed()->get();
            return view('volunteers.index', compact('volunteers'));
        }

        if ($search == '1' || $search == '0') {
            $volunteers = Volunteer::where('status', $search)->get();
            return view('volunteers.index', compact('volunteers'));
        }

        $volunteers = Volunteer::withTrashed()->get();
        return view('volunteers.index', compact('volunteers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Volunteer $volunteer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Volunteer $volunteer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Volunteer $volunteer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Volunteer $volunteer)
    {
        $volunteer->status = 0;
        $volunteer->save();
        if ($volunteer->delete()) {

            return redirect()->route('volunteers.index')->with(['success' => 'The volunteer has been archived']);;
        }
        return redirect()->route('volunteers.index')->with(['error' => 'Something went wrong']);
    }

    public function active(Volunteer $volunteer)
    {

        $volunteer->status = 1;
        if ($volunteer->save()) {

            return redirect()->route('volunteers.index')->with(['success' => 'The volunteer`s status is set to active']);;
        }
        return redirect()->route('volunteers.index')->with(['error' => 'Something went wrong']);
    }

    public function inactive(Volunteer $volunteer)
    {

        $volunteer->status = 0;
        if ($volunteer->save()) {

            return redirect()->route('volunteers.index')->with(['success' => 'The volunteer`s status is set to inactive']);;
        }
        return redirect()->route('volunteers.index')->with(['error' => 'Something went wrong']);
    }
}
