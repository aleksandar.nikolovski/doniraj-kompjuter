<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use App\Models\PartnerType;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StorePartnerRequest;
use App\Http\Requests\UpdatePartnerRequest;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $partners = Partner::all();
        return view('partners.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $partnerTypes = PartnerType::all();

        return view('partners.create', compact('partnerTypes'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePartnerRequest $request)
    {
        $partner = new Partner();
        $partner->name = $request->name;
        $partner->website = $request->website;
        $partner->partner_type_id = $request->partner_type_id;

        $image = $request->file('image');
        $fileName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
        $filePath = 'uploads/' . $fileName;
        Storage::disk('public')->put($filePath, file_get_contents($request->file('image')));
        $partner->image = $filePath;

        if ($partner->save()) {
            return redirect()->route('partners.create')->with(['success' => 'The partner has been created']);
        }

        return redirect()->route('partners.create')->with(['error' => 'Something went wrong']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Partner $partner)
    {
        $partnerTypes = PartnerType::all();
        return view('partners.edit', compact(['partner', 'partnerTypes']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePartnerRequest $request, Partner $partner)
    {
        $partner->name = $request->name;
        $partner->website = $request->website;
        $partner->partner_type_id = $request->partner_type_id;

        if ($request->hasFile('image')) {

            if (Storage::disk('public')->exists($partner->image)) {
                Storage::disk('public')->delete($partner->image);
            }

            $image = $request->file('image');
            $fileName = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $filePath = 'uploads/' . $fileName;
            Storage::disk('public')->put($filePath, file_get_contents($request->file('image')));
            $partner->image = $filePath;
        }

        if ($partner->save()) {
            return redirect()->route('partners.index')->with(['success' => 'The partner has been updated']);
        }

        return redirect()->route('partners.index')->with(['error' => 'Something went wrong']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Partner $partner)
    {
        if (Storage::disk('public')->exists($partner->image)) {
            Storage::disk('public')->delete($partner->image);
        }

        if ($partner->delete()) {
            return redirect()->route('partners.index')->with('success', 'The partner has been deleted successfuly');
        }

        return redirect()->route('partners.index')->with('error', 'Something went wrong...');
    }
}
