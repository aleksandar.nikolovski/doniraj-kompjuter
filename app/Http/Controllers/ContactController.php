<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->has('startDate')) {

            $startDate = Carbon::parse($request->startDate);
            $edDate = Carbon::parse($request->endDate);

            $contacts = Contact::where('created_at', '<=', $edDate)
                ->where('created_at', '>=', $startDate)
                ->get();

            return view('contacts.index', compact('contacts'));
        }

        $search = $request->search;
        if ($search == 'archived') {
            $contacts = Contact::onlyTrashed()->get();
            return view('contacts.index', compact('contacts'));
        }

        if ($search == '1' || $search == '0') {
            $contacts = Contact::where('status', $search)->get();
            return view('contacts.index', compact('contacts'));
        }

        $contacts = Contact::withTrashed()->get();
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contact $contact)
    {
        $contact->status = 0;
        $contact->save();
        if ($contact->delete()) {

            return redirect()->route('contacts.index')->with(['success' => 'The contact has been archived']);;
        }
        return redirect()->route('contacts.index')->with(['error' => 'Something went wrong']);
    }

    public function active(Contact $contact)
    {

        $contact->status = 1;
        if ($contact->save()) {

            return redirect()->route('contacts.index')->with(['success' => 'The contact`s status is set to active']);;
        }
        return redirect()->route('contacts.index')->with(['error' => 'Something went wrong']);
    }

    public function inactive(Contact $contact)
    {

        $contact->status = 0;
        if ($contact->save()) {

            return redirect()->route('contacts.index')->with(['success' => 'The contact`s status is set to inactive']);;
        }
        return redirect()->route('contacts.index')->with(['error' => 'Something went wrong']);
    }
}
