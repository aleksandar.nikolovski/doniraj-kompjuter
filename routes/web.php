<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\VolunteerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::middleware('auth')->group(function () {
    Route::redirect('/', 'admin-dashboard');
    Route::get('admin-dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

    Route::get('/blog-categories',                [CategoryController::class, 'index'])->name('blog.categories.index');
    Route::post('/blog-categories',               [CategoryController::class, 'store'])->name('blog.categories.store');
    Route::get('/blog-categories/{category}/edit',[CategoryController::class, 'edit'])->name('blog.categories.edit');
    Route::put('/blog-categories/{category}',     [CategoryController::class, 'update'])->name('blog.categories.update');
    Route::delete('/blog-categories/{category}',  [CategoryController::class, 'destroy'])->name('blog.categories.destroy');

    Route::get('/blogs',            [BlogController::class, 'index'])->name('blogs.index');
    Route::get('/blogs/create',     [BlogController::class, 'create'])->name('blogs.create');
    Route::post('/blogs',           [BlogController::class, 'store'])->name('blogs.store');
    Route::get('/blogs/{blog}/edit',[BlogController::class, 'edit'])->name('blogs.edit');
    Route::put('/blogs/{blog}',     [BlogController::class, 'update'])->name('blogs.update');
    Route::delete('/blogs/{blog}',  [BlogController::class, 'destroy'])->name('blogs.destroy');

    Route::get('/videos',              [VideoController::class, 'index'])->name('videos.index');
    Route::get('/videos/create',       [VideoController::class, 'create'])->name('videos.create');
    Route::post('/videos',             [VideoController::class, 'store'])->name('videos.store');
    Route::get('/videos/{video}/edit', [VideoController::class, 'edit'])->name('videos.edit');
    Route::put('/videos/{video}',      [VideoController::class, 'update'])->name('videos.update');
    Route::delete('/videos/{video}',   [VideoController::class, 'destroy'])->name('videos.destroy');

    Route::resource('partners', PartnerController::class);

    Route::get('/clients',                   [ClientController::class, 'index'])->name('clients.index');
    Route::post('/clients',                  [ClientController::class, 'store'])->name('clients.store');
    Route::get('/clients/create',            [ClientController::class, 'create'])->name('clients.create');
    Route::delete('/clients/{client}',       [ClientController::class, 'destroy'])->name('clients.destroy');
    Route::post('/clients/{client}/active',  [ClientController::class, 'active'])->name('clients.active');
    Route::post('/clients/{client}/complete',[ClientController::class, 'complete'])->name('clients.complete');
    Route::get('clients/donations',          [ClientController::class, 'donations'])->name('clients.donations');
    Route::post('clients/donate',            [ClientController::class, 'donate'])->name('clients.donate');

    Route::get('volunteers',                       [VolunteerController::class, 'index'])->name('volunteers.index');
    Route::delete('/volunteers/{volunteer}',       [VolunteerController::class, 'destroy'])->name('volunteers.destroy');
    Route::post('/volunteers/{volunteer}/active',  [VolunteerController::class, 'active'])->name('volunteers.active');
    Route::post('/volunteers/{volunteer}/inactive',[VolunteerController::class, 'inactive'])->name('volunteers.inactive');

    Route::get('contacts',                     [ContactController::class, 'index'])->name('contacts.index');
    Route::delete('/contacts/{contact}',       [ContactController::class, 'destroy'])->name('contacts.destroy');
    Route::post('/contacts/{contact}/active',  [ContactController::class, 'active'])->name('contacts.active');
    Route::post('/contacts/{contact}/inactive',[ContactController::class, 'inactive'])->name('contacts.inactive');

});


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
