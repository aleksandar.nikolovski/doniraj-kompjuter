<?php

use App\Http\Controllers\Api\ApiBlogController;
use App\Http\Controllers\Api\ApiClientController;
use App\Http\Controllers\Api\ApiContactController;
use App\Http\Controllers\Api\ApiPartnerController;
use App\Http\Controllers\Api\ApiTokenController;
use App\Http\Controllers\Api\ApiVideoController;
use App\Http\Controllers\Api\ApiVolunteerController;
use App\Http\Controllers\Api\StatisticsController;
use App\Http\Controllers\VolunteerController;
use App\Http\Requests\DonateClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/token', [ApiTokenController::class, 'index']);

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/statistics', [StatisticsController::class, 'index']);
    Route::get('/blogs', [ApiBlogController::class, 'index']);
    Route::get('/videos', [ApiVideoController::class, 'index']);
    Route::get('/partners/{type?}', [ApiPartnerController::class, 'index']);
    Route::post('/clients', [ApiClientController::class, 'store']);
    Route::post('/volunteers', [ApiVolunteerController::class, 'store']);
    Route::post('/contacts', [ApiContactController::class, 'store']);
});
